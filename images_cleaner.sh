#!/bin/bash

# In the current directory:
# - fix images extensions to match the content (eg: image.info -> image.jpg)
# - warn about wrong files not being actual images (eg: 404 html page downloaded as an image)


# get file type
get_type()
{
    file -b "${1}" | cut -d ' ' -f 1
}

# guess extension with file type
guess_ext()
{
    TYPE=$(get_type "$1")
    if [ "${TYPE}" = "JPEG" ]; then
        echo 'jpg'
    elif [ "${TYPE}" = "PNG" ]; then
        echo 'png'
    elif [ "${TYPE}" = "GIF" ]; then
        echo 'gif'
    else
        echo 'ERROR'
    fi
}

# put all wanted files in an array
FILES=( $(find . -maxdepth 1 -type f -exec basename '{}' \;) )

for FILE in "${FILES[@]}"; do
    FILENAME="${FILE%.*}"
    FILENAME_EXT="${FILE##*.}"
    GUESSED_EXT=$(guess_ext "${FILE}")

    if [ "$GUESSED_EXT" = "ERROR" ]; then
        echo "$FILE is not a supported image ($(get_type $FILE))"
        continue
    fi

    if [ "$GUESSED_EXT" != "$FILENAME_EXT" ]; then
        echo "Wrong extension for $FILE, found $GUESSED_EXT, Fixing to $FILENAME.$GUESSED_EXT"
        mv "$FILE" "$FILENAME.$GUESSED_EXT"
    fi
done
