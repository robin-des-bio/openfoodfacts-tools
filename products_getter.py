#!/usr/bin/python3

import importlib
import logging
import pickle
from pasteque_database import get_all_barcodes

openfoodfacts = None
PyMongo = None


def is_from_openfoodfacts_db(product):
    # if a product is not from OpenFoodFacts, it only has a 'code' field
    # _id seems to be always present
    return '_id' in product


def get_products_from_mongodb():
    products = []
    client = PyMongo.MongoClient()  # default host and default port
    db = client.openfoodfacts
    products_db = db.products
    for barcode in get_all_barcodes():
        product = products_db.find_one({'code': barcode})
        if product:
            product['code'] = barcode  # Normalize this field
            products.append(product)
        else:
            products.append({'code': barcode})
    client.close()
    return products


def get_products_from_api():
    products = []
    barcodes = get_all_barcodes()
    for barcode in barcodes:
        product = openfoodfacts.products.get_product(barcode)
        if product["status"]:
            product['product']['code'] = barcode  # Normalize this field
            products.append(product['product'])
        else:
            products.append({'code': barcode})
    return products


def get_products():
    """
    Returns a list of products dictionnaries
    if the product is in OpenFoodFacts DB, the dict is the data present in the DB
    if the product is absent, the dict is just {'code': <barcode_of_the_product>}
    """
    global openfoodfacts
    global PyMongo
    offline_products_file = "openfoodfacts_api_data.pkl"
    try:
        with open(offline_products_file, 'rb') as f:
            products = pickle.load(f)
    except FileNotFoundError:
        with open(offline_products_file, 'wb') as f:
            try:
                openfoodfacts = importlib.import_module('openfoodfacts')
                logging.info("Using openfoodfacts module for openfoodfacts data")
                products = get_products_from_api()
            except ImportError:
                try:
                    PyMongo = importlib.import_module('PyMongo')
                    logging.info("Using PyMongo module for openfoodfacts data")
                    products = get_products_from_mongodb()
                except ImportError:
                    logging.critical("No proper module found, please check the requirements.")
            pickle.dump(products, f, pickle.HIGHEST_PROTOCOL)
    return products


if __name__ == "__main__":
    get_products()
