#!/usr/bin/env python
# coding: utf-8


import get_product_data
import pasteque_database
import products_getter


if __name__ == "__main__":
    all_products = products_getter.get_products()
    # get only products present in OpenFoodFacts
    off_products = [p for p in all_products if products_getter.is_from_openfoodfacts_db(p)]

    products_qty = get_product_data.get_quantity_from_dataset(off_products)

    for barcode in products_qty:
        print('Importing', barcode)
        product = pasteque_database.get_product(barcode)
        if not product:
            print('ERROR')
        else:
            pasteque_database.set_quantity(product, products_qty[barcode]['quantity'], products_qty[barcode]['unit'])
