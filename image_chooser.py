#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
from os import listdir
import os.path
import pasteque_database
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import (
    QMainWindow,
    QWidget,
    QPushButton,
    QApplication,
    QHBoxLayout,
    QLabel,
    QVBoxLayout,
)
from PyQt5.QtGui import QPixmap
import sys


class NoImageFoundException(Exception):
    pass


def is_small_image(image):
    # A small image file name is <barcode>_<x>_small.<ext>
    return image.split(".")[0].split("_")[-1] == "small"


class ProductsManager:
    products = {}
    barcodes = []

    def __init__(self, directory):
        self._product_index = -1
        self.all_images = [
            i for i in listdir(directory) if os.path.isfile(os.path.join(directory, i))
        ]
        for image in [i for i in self.all_images if is_small_image(i)]:
            barcode = image.split(".")[0].split("_")[0]
            if barcode not in self.products:
                product = pasteque_database.get_product(barcode)
                if not product:
                    barcode = barcode.lstrip("0")
                    product = pasteque_database.get_product(barcode)
                    if not product:
                        logging.error(
                            "ERROR with barcode "
                            + barcode
                            + ": image ("
                            + image
                            + ") found but no corresponding product in DB!"
                        )
                        continue
                self.products[barcode] = {"product": product}
            if self.products[barcode]["product"].hasImage:
                continue
            else:
                if barcode not in self.barcodes:
                    self.barcodes.append(barcode)
            if "images" not in self.products[barcode]:
                self.products[barcode]["images"] = []
            self.products[barcode]["images"].append(image)
        if not self.barcodes:
            raise NoImageFoundException("""
ERROR: No image found in this directory.
Expected filenames: <barcode>(_<n>)_small.[jpg|jpeg|gif|png]""")

    @property
    def product_index(self):
        return self._product_index

    @product_index.setter
    def product_index(self, product_index):
        if self.barcodes:
            self._product_index = product_index % len(self.barcodes)

    def get_prev(self):
        self.product_index -= 1
        return self.products[self.barcodes[self.product_index]]

    def get_next(self):
        self.product_index += 1
        return self.products[self.barcodes[self.product_index]]

    def get_current(self):
        return self.products[self.barcodes[self.product_index]]

    def set_current_done(self):
        del(self.barcodes[self.product_index])
        self.product_index -= 1
        if not self.barcodes:
            raise StopIteration


class ClickableImage(QLabel):
    clicked = pyqtSignal()

    def __init__(self, filename, click_function=None):
        super().__init__()
        self.filename = filename
        pixmap = QPixmap(self.filename)
        self.setText(self.filename)
        self.setPixmap(pixmap)
        self.set_activated(False)
        self.setFixedSize(400, 400)

    def set_activated(self, activated=True):
        self.activated = activated
        border = "2px solid black"
        if not self.activated:
            border = "2px dashed grey"
        self.setStyleSheet("border: " + border + ";")


    def is_activated(self):
        return self.activated

    def mousePressEvent(self, ev):
        self.clicked.emit()

    def __str__(self):
        return self.filename


class ImageSelector(QHBoxLayout):
    def __init__(self, images=[]):
        super().__init__()
        self.imglbl = []
        self.refresh(images)

    def refresh(self, images):
        if images:
            for w in self.imglbl:
                self.removeWidget(w)
                w.deleteLater()
        self.imglbl = []
        for img in images:
            lbl = ClickableImage(img)  # , self.labelClicked)
            lbl.clicked.connect(self.labelClicked)
            self.addWidget(lbl)
            self.imglbl.append(lbl)
        if self.imglbl:
            self.imglbl[0].set_activated()

    def labelClicked(self):
        sender = self.sender()
        for i, img in enumerate([str(x) for x in self.imglbl]):
            if img is str(sender):
                self.imglbl[i].set_activated()
            else:
                self.imglbl[i].set_activated(False)

    def get_selected(self):
        for img in self.imglbl:
            if img.is_activated():
                return str(img)


class RdBImageChooser(QMainWindow):
    def __init__(self):
        super().__init__()
        self.products = ProductsManager(".")
        self.initUI()

    def initUI(self):
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)

        btn_prev = QPushButton("< Prev")
        btn_save = QPushButton("Save & continue ●>")
        btn_pass = QPushButton("Pass >")

        btn_prev.clicked.connect(self.buttonPrevClicked)
        btn_save.clicked.connect(self.buttonSaveClicked)
        btn_pass.clicked.connect(self.buttonPassClicked)

        self.product_lbl = QLabel()

        hbox_btn = QHBoxLayout()
        hbox_btn.addWidget(btn_prev)
        hbox_btn.addStretch(1)
        hbox_btn.addWidget(self.product_lbl)
        hbox_btn.addStretch(1)
        hbox_btn.addWidget(btn_save)
        hbox_btn.addWidget(btn_pass)

        self.img_selector = ImageSelector()

        vbox_all = QVBoxLayout()
        vbox_all.addLayout(self.img_selector)
        vbox_all.addStretch(1)
        vbox_all.addLayout(hbox_btn)

        self.widget.setLayout(vbox_all)

        self.statusBar()

        self.setWindowTitle("RdB Image Chooser")
        self.show()
        self.buttonPassClicked()

    def update_image_selector(self, product=None, images=["no_image.png"]):
        self.img_selector.refresh(images)
        if product:
            self.product_lbl.setText("<b>" + product.label + "</b><br />" + product.barcode)

    def buttonClicked(self):
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + " was pressed")

    def buttonPrevClicked(self):
        product = self.products.get_prev()
        self.update_image_selector(product["product"], product["images"])

    def buttonSaveClicked(self):
        product = self.products.get_current()
        pasteque_database.add_image(product["product"], self.img_selector.get_selected())
        self.products.set_current_done()
        self.buttonPassClicked()

    def buttonPassClicked(self):
        product = self.products.get_next()
        self.update_image_selector(product["product"], product["images"])


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = RdBImageChooser()
    sys.exit(app.exec_())
