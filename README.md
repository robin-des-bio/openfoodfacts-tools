This repository contains scripts for scrapping products data from openfoodfacts.

# Requirements
`pip install -r requirements.txt`

# Descriptions

## Standalone scripts
### count_elements.sh
Used to count the number of products you have at least an image for.
### generate_miniatures.sh
For every image (jpg, jpeg, gif and png) in current directory, create a miniature with "_small" suffix.
### get_product_data.py
List products containing specific data.
Check the end of the file for examples.
### get_product_images.py
Download images for all products using OpenFoodFacts images with searx and bing as a failover.
It will download images in a "images" subfolder.
### image_chooser.py
Simple QT GUI to choose and insert images from current directory to products in pasteque database.
Images should be named: `<barcode>(_<n>)(_small).[jpg|jpeg|gif|png]`
### image_cleaner.sh
Clean images files previously downloaded: fix extensions, warn about wrong files.
### import_quantity.py
import quantity data from openfoodfacts to pasteque db


## Libraries
### pasteque_database.py
Library used to gather data from pasteque database.
### product_getter.py
Run it as a standalone script to create a pickle file of openfoodfacts data (the goal here is to only ask the api once and then work offline).
You don't have to run it, other scripts that need openfoodfacts data will do it automatically.
### unit_reader.py
Library used to read and standardise quantities (grams or liters).


# Usages
## common init

You must have access to a pasteque database. Then modify the engine string from pasteque_database.py, around line 14.
Default:
~~~~
# Modify this line to match your parameters
engine = create_engine("mysql+mysqlconnector://pasteque:pasteque@localhost/pasteque-api")
~~~~
Read SQLAlchemy's [create_engine documentation][create_engine_doc] for more info on the engine string

[create_engine_doc]: http://docs.sqlalchemy.org/en/latest/core/engines.html


You also must have access to either the OpenFoodFacts API (via the openfoodfacts module), or by importing the mongodb dump to your local mongodb database.

## download and set images
~~~~
$ mkdir images
$ cd images
$ ../get_product_images.py
$ ../images_cleaner.sh
$ ../count_elements.sh  # you can count the number of products for which you have at least an image
$ ../generate_miniatures.sh
$ ../image_chooser.py
~~~~
What it does:
 * download images, one from OpenFoodFacts db or at most 3 from a search engine (searcx or bing)
 * clean images filenames and remove bad files
 * generate miniatures for every downloaded image
 * use the gui to select the correct image for a product (and insert it in pasteque db)

## partially set quantity information
~~~~
$ ./import_quantity.py
~~~~
What it does:
 * only works with products from pasteque db that are present in OpenFoodFacts db
 * get quantity information from OpenFoodFacts db and standardiize it
 * save this data into pasteque db
