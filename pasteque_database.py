#!/usr/bin/env python3
# coding: utf-8

import mimetypes
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError


Base = automap_base()

# Modify this line to match your parameters
engine = create_engine("mysql+mysqlconnector://pasteque:pasteque@localhost/pasteque-api")

Base.prepare(engine, reflect=True)

# mapped classes are now created with names by default
# matching that of the table name.
Products = Base.classes.products
Images = Base.classes.images

session = Session(engine)


def get_all_barcodes():
    res = []
    for product in session.query(Products).filter(Products.barcode > 10000):
        res.append(product.barcode)
    return res


def get_all_products():
    res = []
    for product in session.query(Products).filter(Products.barcode > 10000):
        res.append(product)
    return res


def get_product(barcode):
    return session.query(Products).filter_by(barcode=barcode).first()


def add_image(product, image_path):
    print("saving", image_path, "for", product.barcode)
    with open(image_path, "rb") as image:
        try:
            session.add(Images(model='product',
                               modelId=product.id,
                               mimeType=mimetypes.guess_type(image_path)[0],
                               image=image.read()))
            product.hasImage = 1
            session.commit()
        except SQLAlchemyError:
            session.rollback()


def set_quantity(product, quantity, unit):
    """unit must be g or L"""
    if unit == 'g':
        scaleType = 1
    elif unit == 'L':
        scaleType = 2
    else:
        raise ValueError("unit must be 'g' or 'L', not " + unit)
    try:
        product.scaleType = scaleType
        product.scaleValue = quantity
        session.commit()
    except SQLAlchemyError:
        session.rollback()


def get_image(product):
    return session.query(Images).filter_by(model='products',
                                           modelId=product.id).first().image

