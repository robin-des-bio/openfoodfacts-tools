#!/bin/bash


# Count number of unique elements in current directory
#
# Example:
# $ ls
# ./
# aaa_0.jpg
# aaa_1.jpg
# aaa_2.jpg
# bbb_0.jpg
# count_elements.sh
# $ ./count_elements.sh
# 2
#

function get_all_elements
{
    # put all wanted files in an array
    files=( $(find . -maxdepth 1 -type f -iregex ".*\.\(jpg\|gif\|png\|jpeg\)" -exec basename '{}' \;) )

    for file in "${files[@]}"; do
        filename="${file%.*}"
        echo "${filename%%_*}"
    done | sort | uniq
}

get_all_elements | wc -l
