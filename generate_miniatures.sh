#!/bin/bash


# For every image (jpg, jpeg, gif and png) in current directory, create a miniature with "_small" suffix

RESIZE_DIMENSION="400x400"
QUALITY=60


# put all wanted files in an array
files=( $(find . -maxdepth 1 -type f -iregex ".*\.\(jpg\|gif\|png\|jpeg\)" -exec basename '{}' \;) )

for file in "${files[@]}"; do
    echo -n "${file}"
    convert "${file}" \
            -resize ${RESIZE_DIMENSION}\> \
            -quality ${QUALITY} \
            "${file%.*}_small.${file##*.}"
    echo " OK"
done
