#!/usr/bin/python3

import json
import re
import socket
import ssl
import sys
import urllib.request
import urllib.error

import products_getter


class NoImage(Exception):
    """
    Exception raised in case of image download fail
    """
    pass


def download_image(url, img_name):
    """
    Raise NoImage if unable to download the image
    """
    headers = {'User-Agent':
               'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'}
    extension = (url
                 .split(".")[-1]
                 .split("/")[0]
                 .split("?")[0]
                 .split("&")[0])
    output_file = "%s.%s" % (img_name, extension)
    # set correct encoding for url
    scheme, netloc, path, query, fragment = urllib.parse.urlsplit(url)
    path = urllib.parse.quote(path)
    url = urllib.parse.urlunsplit((scheme, netloc, path, query, fragment))
    # set context to disable ssl verification (avoids 'bad domain' errors)
    ctx_no_ssl = ssl.create_default_context()
    ctx_no_ssl.check_hostname = False
    ctx_no_ssl.verify_mode = ssl.CERT_NONE
    try:
        request = urllib.request.Request(url, None, headers)
        image = urllib.request.urlopen(request, context=ctx_no_ssl).read()
        with open('images/' + output_file, 'wb') as imagefile:
            imagefile.write(image)
    except (socket.timeout, urllib.error.HTTPError,
            urllib.error.URLError, ConnectionResetError):
        raise NoImage


def download_images_from_searx(barcode, max_results=3):
    """
    Return False if no image is downloaded
    """
    # Alternative URLs:
    # url = ('https://search.biboumail.fr?q='
    # url = ('https://searx.aquilenet.fr?q='
    url = ('https://search.casperlefantom.net?q='
           + barcode + '&categories=images&language=fr&format=json')
    try:
        req = urllib.request.Request(url)
        with urllib.request.urlopen(req) as response:
            page_content = response.read()
            encoding = response.info().get_content_charset('utf_8')
            results = json.loads(page_content.decode(encoding))
    except socket.timeout:
        return False
    res_len = min(len(results['results']), max_results)
    success = False
    for i in range(res_len):
        try:
            download_image(results['results'][i]['img_src'], "%s_%i" % (barcode, i))
            success = True
        except NoImage:
            pass
    return success


def download_images_from_bing(barcode, max_results=3):
    """
    Return False if no image is downloaded
    url from https://github.com/ostrolucky/Bulk-Bing-Image-downloader.git
    """
    url = ('https://www.bing.com/images/async?q='
           + barcode + '&count=3&adlt=on')
    req = urllib.request.Request(url)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf8')
        links = re.findall('murl&quot;:&quot;(.*?)&quot;', html)
    res_len = min(len(links), max_results)
    success = False
    for i in range(res_len):
        try:
            download_image(links[i], "%s_%i" % (barcode, i))
            success = True
        except NoImage:
            pass
    return success


def __main__():
    products = products_getter.get_products()
    for i, product in enumerate(products):
        print(i, "/", len(products))
        try:
            download_image(product["image_url"], product["code"])
        except (KeyError, NoImage):
            # No image in OpenFoodFacts, use a fallback
            if not download_images_from_searx(product["code"]):
                if not download_images_from_bing(product["code"]):
                    print("No image found for", product["code"], file=sys.stderr)
            continue


if __name__ == "__main__":
    __main__()
