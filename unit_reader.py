#!/usr/bin/python3

"""
Functions to extract and unify quantity values from openfoodfact data structure
"""

import itertools


# units possible representations in lowercase
GRAMS = ['g', 'gr', 'gramme', 'grammes']
KILOGRAMS = ['kg']
LITERS = ['l', 'litre', 'litres', 'liters', 'liter']
MILILITERS = ['ml']
CENTILITERS = ['cl']

# words to ignore from quantity field, lowercase
UNWANTED_WORDS = ['minimum',
                  ' e',
                  'poids net total',
                  'poids net :',
                  'net :']


def uniform_unit(value, unit):
    if unit in GRAMS:
        unit = 'g'
    elif unit in KILOGRAMS:
        unit = 'g'
        value = value * 1000
    elif unit in LITERS:
        unit = 'L'
    elif unit in MILILITERS:
        unit = 'L'
        value = value / 1000
    elif unit in CENTILITERS:
        unit = 'L'
        value = value / 100
    else:
        raise ValueError
        # unit = 'UNKNOWN_UNIT(' + quantity + ')'
    return(value, unit)


def guess_quantity(quantity):
    """
    quantity is from OpenFoodFacts field of the same name
    Return a tuple (value as float, unit as str)
    """
    qty = quantity.strip().lower()
    if not qty:
        raise ValueError

    # remove unwanted words
    for word in UNWANTED_WORDS:
        i = qty.find(word)
        if i != -1:
            qty = qty[:i] + qty[i + len(word):]

    # clean string
    qty = qty.replace(',', '.')  # decimals separator
    qty = qty.replace('*', 'x')  # multiplication sign
    qty = qty.replace(' ', '')   # remove spaces

    # separate numbers from text
    res = [''.join(x) for _, x in itertools.groupby(qty, str.isalpha)]

    # multiply when needed (eg: 2 x 100 g)
    if len(res) > 1 and res[1] == 'x':
        res = [str(float(res[0]) * float(res[2])), res[3]]

    if len(res) == 1:  # no unit
        raise ValueError

    # remove trailing '.' in unit field
    res[1] = res[1].rstrip('.')

    # convertion to float might raise ValueError
    return uniform_unit(float(res[0]), res[1])



def guess_quantity_from_name(name):
    def is_from_float(x):
        return x.isdigit() or x == '.'

    n = name.strip().lower()
    if not n:
        raise ValueError

    n = n.replace(',', '.')  # decimals separator
    n = n.replace('*', 'x')  # multiplication sign

    array = [''.join(x) for _, x in itertools.groupby(n, is_from_float)]
    for i, word in enumerate(array):
        # test if we have a numeric value
        try:
            possible_value = float(word)
        except ValueError:
            continue
        # manage value
        try:
            if array[i + 1].strip() == 'x':
                value = possible_value * float(array[i + 2])
                possible_unit = array[i + 3]
            else:
                value = possible_value
                possible_unit = array[i + 1]
        except IndexError:
            continue
        # manage unit
        try:
            if possible_unit.strip():
                unit = possible_unit.split()[0].rstrip(')')
                return uniform_unit(value, unit)
        except ValueError:
            continue
    # No match, we fail
    raise ValueError
