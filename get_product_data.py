#!/usr/bin/python3

import products_getter
from pprint import pprint

from unit_reader import guess_quantity, guess_quantity_from_name


def print_attribute_from_dataset(attribute, dataset):
    no_attribute = []
    for product in dataset:
        if attribute in product and product[attribute]:
            print(product['code'], ':', product[attribute])
        else:
            no_attribute.append(product)
    print(len(no_attribute), "products without", attribute,
          '(total', len(dataset), ')')


def get_quantity_from_dataset(dataset):
    res = {}
    for i, product in enumerate(dataset):
        (value, unit) = (0., 'UNKNOWN_UNIT')
        try:
            (value, unit) = guess_quantity(product['quantity'])
        except (KeyError, ValueError):
            try:
                (value, unit) = guess_quantity_from_name(product['product_name'])
            except (KeyError, ValueError):
                # could not guess quantity for this product
                continue
        res[product['code']] = {'quantity': value, 'unit': unit}
    return res


def print_quantity_from_dataset(dataset):
    products = get_quantity_from_dataset(dataset)
    for barcode in products:
        print(barcode.rjust(13, ' '), ': ',
              str((products[barcode]['quantity'], products[barcode]['unit'])).rjust(15, ' '))


def has_info(element, info):
    def is_info_inside(x):
        return info.lower() in x.lower()

    def recurse_check(e):
        if type(e) is str:
            if is_info_inside(e):
                return True
        elif type(e) is list:
            for x in e:
                if recurse_check(x):
                    return True
        elif type(e) is dict:
            for x in e.values():
                if recurse_check(x):
                    return True
        else:
            pass  # print('ERROR: unmanaged type', type(e))
        return False

    return recurse_check(element)


def print_all_with_info_from_dataset(info, dataset, extra_print=False):
    total = 0
    for product in dataset:
        if has_info(product, info):
            print(product['code'])
            total += 1
        elif extra_print:
            pprint(product)
    if extra_print:
        print(total, "products are", info, "(on", len(dataset), ")")


def print_all_with_info_list_from_dataset(info_list, dataset, extra_print=False):
    total = 0
    for product in dataset:
        has_infos = False
        for info in info_list:
            if has_info(product, info):
                print(product['code'])
                total += 1
                has_infos = True
                break
        if extra_print and not has_infos:
            pprint(product)
    if extra_print:
        print(total, "products are", " or ".join(info_list), "(on", len(dataset), ")")


if __name__ == "__main__":
    all_products = products_getter.get_products()
    # get only products present in OpenFoodFacts
    products = [p for p in all_products if products_getter.is_from_openfoodfacts_db(p)]

    # print_attribute_from_dataset('product_name', products)
    # print_attribute_from_dataset('labels', products)
    # print_attribute_from_dataset('ingredients_that_may_be_from_palm_oil_n', products)
    # print_attribute_from_dataset('quantity', products)
    print_quantity_from_dataset(products)

    # print_all_with_info_from_dataset("vegan", products, True)
    # print_all_with_info_list_from_dataset(["vegan", "vegetalien"], products, True)
    # print_all_with_info_list_from_dataset(["bio", "organic"], products, True)
